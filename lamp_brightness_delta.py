import sys

from lifxlan import Light

kwargs = dict(arg.split('=') for arg in sys.argv[1:])

l = Light("d0:73:d5:58:61:6e", "192.168.1.7")

if l:
    print(l.get_color())
    l.set_brightness(float(kwargs["delta"]) + l.get_color()[2], 200, True)
