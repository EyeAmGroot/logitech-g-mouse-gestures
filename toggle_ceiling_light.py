from requests import get, post

hass_url = "http://192.168.1.12:8123"
token = open('auth_token.txt', 'r').read()

url_get = f"{hass_url}/api/states/light.office_ceiling"

headers = {
    "Authorization": f"Bearer {token}",
    "content-type": "application/json"
}

response = get(url_get, headers=headers)
state = response.json()["state"]

if state == "on":
    url_post = f"{hass_url}/api/services/light/turn_off"
else:
    url_post = f"{hass_url}/api/services/light/turn_on"

data = {
    "entity_id":"light.office_ceiling"
}

response = post(url_post, headers=headers, json=data)