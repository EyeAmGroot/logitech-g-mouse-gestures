from requests import get, post

hass_url = "http://192.168.1.12:8123"
token = open('auth_token.txt', 'r').read()

headers = {
    "Authorization": f"Bearer {token}",
    "content-type": "application/json"
}

url_post = f"{hass_url}/api/services/switch/toggle"

data = {
    "entity_id":"switch.sonoff_a4800aadf3"
}

response = post(url_post, headers=headers, json=data)