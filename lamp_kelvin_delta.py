import sys

from lifxlan import Light

kwargs = dict(arg.split('=') for arg in sys.argv[1:])

l = Light("d0:73:d5:58:61:6e", "192.168.1.7")

try:
    l.set_colortemp(int(kwargs["delta"]) + l.get_color()[3], 200, True)
except:
    print("No light found")
